from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.recoco import Timer
from collections import defaultdict
from pox.openflow.discovery import Discovery
from pox.lib.util import dpid_to_str
import time
from pox.lib.util import dpidToStr
from pox.openflow.of_json import *

#Script used to sniff the incoming flows & plasmating them in a file
class tableStats(EventMixin):
  #Class constructor, defines essential values
  def __init__(self,interval = 10,fileName='fmega'):
    #Used list to gather tableStats
    self.tableActiveCount = {}
    #Number of seconds between each request
    self.interval = interval
    core.openflow.addListeners(self)
    self.log = core.getLogger()
    #Creating or open the file
    self.file=open ('sniffeo/'+fileName+'.csv','a')
    self.file.write(str(self.armar_Nombre_Columas())+"\n")
    #self.file.close()

  #Parsing method, from match object attributes to a list
  def armar_Match(self,match):
      lista_match=[str(bin(match.wildcards)),str(match.__getattr__('in_port'))]
      lista_match.append(str(match.__getattr__('dl_src')))
      lista_match.append(str(match.__getattr__('dl_dst')))
      lista_match.append(str(match.__getattr__('dl_vlan')))
      lista_match.append(str(match.__getattr__('dl_type')))
      lista_match.append(str(match.__getattr__('nw_tos')))
      lista_match.append(str(match.__getattr__('nw_proto')))
      lista_match.append(str(match.__getattr__('nw_src')))
      lista_match.append(str(match.__getattr__('nw_dst')))
      lista_match.append(str(match.__getattr__('tp_src')))
      lista_match.append(str(match.__getattr__('tp_dst')))
      return lista_match

  #Parsing method, from the entire flow to a list
  def armar_Flow(self,stats):
      flujo=[str(stats.__len__()),str(stats.table_id)]
      flujo+=self.armar_Match(stats.match)
      flujo.append(str(stats.duration_sec))
      flujo.append(str(stats.duration_nsec))
      flujo.append(str(stats.priority))
      flujo.append(str(stats.idle_timeout))
      flujo.append(str(stats.hard_timeout))
      flujo.append(str(stats.cookie))
      flujo.append(str(stats.packet_count))
      flujo.append(str(stats.byte_count))
      #s=','.join(flujo);
      return flujo

  #Method used to write the initial columns in the .csv file
  def armar_Nombre_Columas(self):
      th="Length,Table_id,Wilcards,in_port,dl_src,dl_dst,dl_vlan,dl_type,nw_tos,"
      th+="nw_proto,nw_src,nw_dst,tp_src,tp_dst,duration_sec,duration_nsec,"
      th+="priority,idle_timeout,hard_timeout,cookie,packet_count,byte_count"
      return th

  #The fired method when a switch connection is detected, occurs automatically
  def _handle_ConnectionUp(self,event):
    print "Switch %s has connected" %event.dpid
    #Uncomment this to receive in console table stats
    #self.sendTableStatsRequest(event)
    #Sends first request to the connected switch
    self.sendFlowStatsRequest(event)

  #Method for handling TableStats responses from the switch, occurs automatically
  def _handle_TableStatsReceived(self,event):
    sw = 's%s'%event.dpid
    self.tableActiveCount[sw] = event.stats[0].active_count
    print "TableStatsReceived"
    print self.tableActiveCount
    print event.stats[0].show()
    #Timer to send a TableStats request in the defined interval of seconds
    Timer(self.interval, self.sendTableStatsRequest,args=[event])

  #Method for sending a TableStat request to a switch
  def sendTableStatsRequest(self,event):
    sr = of.ofp_stats_request()
    sr.type = of.OFPST_TABLE
    #Sends previously created request
    event.connection.send(sr)
    print "Send table stat message to Switch %s " %event.dpid

  def sendFlowStatsRequest(self,event):
    #sr = of.ofp_stats_request()
    #sr.type = of.ofp_flow_stats_request()
    #Exectures the last 2 commented lines in only one
    event.connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))
    #event.connection.send(sr)

  def _handle_FlowStatsReceived (self,event):
    #Uncomment this for debugging purposes
    #self.log.debug("FlowStatsReceived from %s: %s",dpidToStr(event.connection.dpid), stats)
    for f in event.stats:
      lista_flujo= self.armar_Flow(f) #Single flow, converted to a list
      str_flujo=','.join(lista_flujo) #Flow converted to string
      self.file.write(str_flujo+"\n") #Write the generated string to the file
      #Uncomment this for debugging purposes
      #print f.show()
    #Set timer to call again a FlowStats request
    Timer(self.interval, self.sendFlowStatsRequest,args=[event])



#Launching method, registers the previous class as a pox component
def launch(interval = '10',fileName='f'):
  interval = int(interval)
  core.registerNew(tableStats,interval,fileName)
