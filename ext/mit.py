from pox.core import core
import pox.lib.packet as pkt
from pox.lib.packet import *
from pox.lib.revent import *
from pox.lib.packet.ipv4 import ipv4
from collections import defaultdict
from pox.lib.addresses import IPAddr, IPAddr6, EthAddr
import pox.openflow.libopenflow_01 as of
from pox.openflow.discovery import Discovery
from pox.lib.recoco import Timer
import re
import subprocess
import random
#import tensorflow as tf
import time
import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle
import joblib
from sklearn.svm import SVC
#Attack mitigation script: Can generate .cvs files for IA training or implement flow rules to the network 
class tableStats(EventMixin):
  #Load the trained model
  filename1 = '/root/Desktop/poxu/ext/model_svm_linear.sav'
  #model = tf.keras.models.load_model('/root/Desktop/poxu/ext/saved_model/my_model')
  model1 = joblib.load(filename1)
  filename2 = '/root/Desktop/poxu/ext/model_random_forest_vanilla.sav'
  #model = tf.keras.models.load_model('/root/Desktop/poxu/ext/saved_model/my_model')
  model2 = joblib.load(filename2)
  #print ((model1.predict([[3,282000000,3,174,5711501,1578]])))
  #print ((model2.predict([[3,282000000,3,174,5711501,1578]])))
  total_flows_correct = 0.0
  total_flows_bad = 0.0
  cooldown_rf = 50

  #model.summary()
 #Interval: number of seconds between each FlowsStats query
  #writeMode: Boolean used to change the main purpose: write training files or mitigate attacks

  def __init__(self,interval = 10,writeMode=False,filename='f'):
    self.interval = interval
    self.infected_counter={}
    self.hosts_byte_count={}
    core.openflow.addListeners(self)
    self.start_time= time.time()
    self.log = core.getLogger()

    self.writeMode=writeMode
    #In case the writeMode was activated, created a .csv file
    if(self.writeMode):
        self.file=open ('sniffeo/'+filename+'.csv','a')
        self.file.write(str(self.armar_Nombre_Columas())+"\n")
  #Method used to classify the traffic
  def is_randomly_attacking(self,lista_flujo):

      if (lista_flujo[10] != "10.0.0.1"):

	      #model = self.model
	  
	      total_flows_correct = self.total_flows_correct
	      total_flows_bad = self.total_flows_bad
	      model1 = self.model1
	      model2 = self.model2
	      listp = []
	      listn = []
	      list_atac_accuracy = ["10.0.0.2","10.0.1.2"]
		
	      
	      #print("MIT: Imprimiendo flujo en is_randomly_attacking...")
	      column_names=['duration_sec','duration_nsec','packet_count','byte_count','bytes_total','current_runtime','flow_status']
	     #feature_names = column_names[:-1]
	      #label_name = column_names[-1]
	      #print("Features:{}".format(feature_names))
	      #print("label:{}".format(label_name))
	      #class_names = ['attack', 'normal']
	     #print(lista_flujo)

	  
	      listp.append(lista_flujo[14])
	    
	      listp.append(lista_flujo[15])
	      listp.append(lista_flujo[20])
	      listp.append(lista_flujo[21])
	      listp.append(lista_flujo[22])
	      listp.append(lista_flujo[23])
	      

	      #print(listp)
	      i=0
	      for s in listp:
		if s == "None":
		  listp[i] = '0'
		listn.append(float(listp[i]))
		i = i + 1
	      #print(listn)
	      print(lista_flujo[10])

	      #predict modelo 1 SVM Linear
	      pred = model1.predict([listn])
	      #predict modelo 2 RF vainilla
	      """if (self.cooldown_rf >= 50): 
				
		pred = model2.predict([listn])
		if(pred == 0):
			self.cooldown_rf = 50
		else:		
			self.cooldown_rf = 0
	      else: 
		pred = [1]
		self.cooldown_rf = self.cooldown_rf + 1
	      """
              print(pred)
	
	      if (lista_flujo[10] in list_atac_accuracy):
	      	
	      	
	      	if (pred[0] != 0):
	      		self.total_flows_bad= self.total_flows_bad + 1.0
		else:
			self.total_flows_correct = self.total_flows_correct +1.0	
	      else:
		if(pred[0] == 1):
			self.total_flows_correct = self.total_flows_correct +1.0
		else:
			self.total_flows_bad = self.total_flows_bad + 1.0
		
	      #print(model2.predict([listn]))
	      print("\n")
	      
	      #predict_dataset = tf.convert_to_tensor([listn])

	      #predictions = model(predict_dataset, training=False)
	      #for i, logits in enumerate(predictions):
		#class_idx = tf.argmax(logits).numpy()
		#p = tf.nn.softmax(logits)[class_idx]
		#name = class_names[class_idx]
		#print("Example {} prediction: {} ({:4.1f}%)".format(i, name, 100*p))
	      total = self.total_flows_correct + self.total_flows_bad
	      c = self.total_flows_correct
	      b = self.total_flows_bad
	      p = self.total_flows_correct/total
	      print('total:' + str(total))
	      print('correct:'+ str(c))
	      print('bad:'+ str(b))
	      print('percent'+ str(p))
	      if(pred[0] ==  0): #10% de estar atacando, valor usado para probar
		return True
	      else:
		return False
	
      else :
	      return False
    #Transform match object to a list

  def armar_Match(self,match):
      lista_match=[str(bin(match.wildcards)),str(match.__getattr__('in_port'))]
      lista_match.append(str(match.__getattr__('dl_src')))
      lista_match.append(str(match.__getattr__('dl_dst')))
      lista_match.append(str(match.__getattr__('dl_vlan')))
      lista_match.append(str(match.__getattr__('dl_type')))
      lista_match.append(str(match.__getattr__('nw_tos')))
      lista_match.append(str(match.__getattr__('nw_proto')))
      lista_match.append(str(match.__getattr__('nw_src')))
      lista_match.append(str(match.__getattr__('nw_dst')))
      lista_match.append(str(match.__getattr__('tp_src')))
      lista_match.append(str(match.__getattr__('tp_dst')))
      return lista_match
  #Transform the entire flow to a list
  def armar_Flow(self,stats):
      flujo=[str(stats.__len__()),str(stats.table_id)]
      flujo+=self.armar_Match(stats.match)
      flujo.append(str(stats.duration_sec))
      flujo.append(str(stats.duration_nsec))
      flujo.append(str(stats.priority))
      flujo.append(str(stats.idle_timeout))
      flujo.append(str(stats.hard_timeout))
      flujo.append(str(stats.cookie))
      flujo.append(str(stats.packet_count))
      flujo.append(str(stats.byte_count))

      # New custom field: total byte count per host
      flujo.append(str(self.hosts_byte_count[str(stats.match.__getattr__('nw_src'))]))
      flujo.append(str(time.time()-self.start_time))
      #s=','.join(flujo);
      return flujo
  #Building the initial column of the .csv file
  def armar_Nombre_Columas(self):
      th="Length0,Table_id1,Wilcards2,in_port3,dl_src4,dl_dst5,dl_vlan6,dl_type7,nw_tos8,"
      th+="nw_proto9,nw_src10,nw_dst11,tp_src12,tp_dst13,duration_sec14,duration_nsec15,"
      th+="priority16,idle_timeout17,hard_timeout18,cookie19,packet_count20,byte_count21, bytes_total22,current_runtime23"
      return th
  #Sending the first FlowStatsRequest to the connected Switch
  def _handle_ConnectionUp(self,event):
    print "Switch %s has connected" %event.dpid
    self.sendFlowStatsRequest(event)

  def sendFlowStatsRequest(self,event):
    event.connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))
  #Method used for the mitigation logic
  def _handle_FlowStatsReceived (self,event):
    #Iterate through all the received flows 
    for f in event.stats:

      src_ip= str(f.match.__getattr__('nw_src') ) #Source IP
      if src_ip in self.hosts_byte_count:
          self.hosts_byte_count[src_ip]+=f.byte_count
      else:
          self.hosts_byte_count[src_ip]=f.byte_count

      lista_flujo= self.armar_Flow(f) #List-format Single flow 
      #string_flujo=','.join(lista_flujo) #String converted...
      if self.writeMode:
          print("Escribiendo en archivo...")
          str_flujo=','.join(lista_flujo)
          self.file.write(str_flujo+"\n")
      elif self.is_randomly_attacking(lista_flujo): #Using the method to classify the flow
          if src_ip not in self.infected_counter: #Adding the infected IP to the list
              self.infected_counter[src_ip]=0.1 #Initial chance to drop a packet from that IP: 10%
              print "MIT: Atacante 'detectado': "+src_ip

          if self.infected_counter[src_ip] >=1: #When the drop chance reaches 100%, installs a flow rule
                print("MIT: Bloqueo bloqueo bloqueo: "+src_ip) #Building the flow rule
                msg = of.ofp_flow_mod()
                msg.match.dl_type = f.match.__getattr__('dl_type')
                msg.match.nw_src = IPAddr(src_ip)
                msg.match.nw_proto= f.match.__getattr__('nw_proto')
                msg.match.tp_dst = f.match.__getattr__('tp_dst')
                #msg.match.idle_timeout =10
                msg.hard_timeout=20 #Modify this value to alter the rule expiration time
                msg.actions.append(of.ofp_action_output(port = of.OFPP_NONE)) 
                event.connection.send(msg) #Sending the flow rule to the Switch
                self.infected_counter[src_ip]=0.0
                #event.halt=True
          else:
                if self.infected_counter[src_ip]<1.0: #If the host is detected again as an infected, raise +5% it's drop chance
                    self.infected_counter[src_ip]+=0.05
                    print "MIT: Flujo de "+src_ip+" detectado, probabilidad actualizada: "+str(self.infected_counter[src_ip])
    #Make another flowstat query in x seconds
    Timer(self.interval, self.sendFlowStatsRequest,args=[event])
  #Method to randomly drop the infected hosts packets
  def _handle_PacketIn(self,event):
      packet= event.parse()
      ip_packet= packet.find("ipv4")
      if isinstance(ip_packet,ipv4):
          src_ip= str( ip_packet.srcip )
          if src_ip in self.infected_counter:
              num= random.uniform(0,1)
              if num <= self.infected_counter[src_ip]: #If the random number is below the drop chance, drop the packet
                  print "MIT: Paquete de "+src_ip+" bloqueado manualmente "+str(num)+"< "+str(self.infected_counter[src_ip])
                  event.halt=True #Bloquing the packet
              #else: #Uncomment both lines for debugging purposes
                #  print "Mit: NO se bloqueo el supuesto "+src_ip+" por: "+str(num)+" not < "+str(self.infected_counter[src_ip])
  #Print a log message when a flow rule has been expired
  def _handle_FlowRemoved(self,event):
      print "MIT: Regla de flujo borrada"


#Launching method, registers this file as a pox component
def launch(interval = '10',writeMode=False,filename='f'):
  interval = int(interval)
  writeMode= bool(writeMode)
  core.registerNew(tableStats,interval,writeMode,filename)
