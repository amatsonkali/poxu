from __future__ import absolute_import, division, print_function, unicode_literals
import os 
#import matplotlib.pyplot as plt
import tensorflow as tf

model = tf.keras.models.load_model('model_first_dataset_random_forest.h5')

model.save('saved_model/my_model') 
new_model = tf.keras.models.load_model('saved_model/my_model')

# Check its architecture
new_model.summary()

